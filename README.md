# MySQL Module v0.3
MySQL bind is a third-party module,  which provides Squirrel functions to access data from MySQL databases. More information about MySQL database you will find on the internet. If you looking for SQL language tutorial look here: [http://www.w3schools.com/sql/](https://www.w3schools.com/sql/)

**Used SDK**: https://downloads.mysql.com/archives/c-c/

# Usage
To load this module, you have to add reference in `scripts.xml` file.
```xml
<!-- For windows -->
<module src="MySQL.dll" type="server" />
<!-- For linux -->
<module src="./MySQL.so" type="server" />
```

## Binded functions
Currently, the module support the following functions:
* `null`/`userpointer` handler = mysql_connect(`string` host, `string` user, `string` password, `string` database, `int` port = 3306)
* `void` mysql_close(`userpointer` handler)
* `null`/`userpointer` result = mysql_query(`userpointer` handler, `string` query)
* `null`/`int` rowCount = mysql_num_rows(`userpointer` result)
* `null`/`int` fieldCount = mysql_num_fields(`userpointer` result)
* `null`/`array` data = mysql_fetch_row(`userpointer` result)
* `null`/`table` data = mysql_fetch_assoc(`userpointer` result)
* `bool` ping = mysql_ping(`userpointer` handler)
* `void` mysql_free_result(`userpointer` result)
* `null`/`string` error = mysql_error(`userpointer` handler)
* `null`/`int` errorId = mysql_errno(`userpointer` handler)
* `null`/`string` escaped = mysql_real_escape_string(`userpointer` handler, `string` text)


## Example code usage
```lua
local handler = mysql_connect("localhost", "root", "", "db_test");
if (handler)
{
	local result = mysql_query(handler, "SELECT * FROM mdb_users");
 
	if (result)
	{
		print("Query done " + mysql_num_rows(result));
		local row = mysql_fetch_row(result);
		if (row)
		{
			foreach (val in row)
			{
				print(type(val) + " " + val);
			}
		}
 
		mysql_free_result(result);
	}
	else
	{
		print(mysql_error(handler));
		print("Error ID: " + mysql_errno(handler));
	}
 
	result = mysql_query(handler, "SELECT * FROM mdb_users");
 
	if (result)
	{
		print("Query done " + mysql_num_fields(result));
		local row_assoc = mysql_fetch_assoc(result);
		if (row_assoc)
		{
			print(row_assoc["username"] + " " + row_assoc["email"]);
		}
 
		mysql_free_result(result);
	}
	else
	{
		print(mysql_error(handler));
		print("Error ID: " + mysql_errno(handler));
	}
 
	mysql_close(handler);
}
```